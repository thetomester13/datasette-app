#!/bin/bash

# Cannot use that due to virtual env issues
# set -eu -o pipefail

cd /app/data/datasette-root

echo "=> Ensure directories"
mkdir -p /app/data/datasette-root/templates /app/data/datasette-root/plugins /app/data/datasette-root/static

echo " => Changing ownership"
chown -R cloudron:cloudron /app/data

echo "=> Get secret key"
if [[ ! -f /app/data/.secret_key ]]; then
  python3 -c 'import secrets; print(secrets.token_hex(32))' > /app/data/.secret_key
fi
SECRET_KEY=$(</app/data/.secret_key)

echo "=> Starting datasette"
exec /usr/local/bin/gosu cloudron:cloudron datasette serve /app/data/datasette-root/ -h 0.0.0.0 -p 8000
