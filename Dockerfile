FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=0.56

RUN pip3 install datasette==${VERSION}

# COPY local.py /app/code/taiga-back/settings/config.py
COPY start.sh /app/code/start.sh

RUN chmod +x /app/code/start.sh

EXPOSE 8000

CMD [ "/app/code/start.sh" ]
